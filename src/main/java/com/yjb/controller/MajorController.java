package com.yjb.controller;

import com.github.pagehelper.PageInfo;
import com.yjb.entity.Major;
import com.yjb.response.PageResult;
import com.yjb.response.Result;
import com.yjb.response.StatusCode;
import com.yjb.service.MajorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * (Major)控制层
 *
 * @author protagonist
 * @since 2021-01-18 20:46:16
 */
@RestController
@Slf4j
@RequestMapping("/major")
public class MajorController {
    /**
     * 服务对象
     */
    @Resource
    private MajorService majorServiceImpl;

    /**
     * 通过主键查询单条数据
     *
     * @param mId 主键
     * @return 单条数据
     */
    @GetMapping(value = "/get/{mId}")
    public Result selectOne(@PathVariable("mId") Integer mId) {
        Major result = majorServiceImpl.selectById(mId);
        if (Objects.nonNull(result)) {
            return new Result<>(true, StatusCode.OK, "查询成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "查询失败");
    }

    /**
     * 新增一条数据
     *
     * @param major 实体类
     * @return Result对象
     */
    @PostMapping(value = "/insert")
    public Result insert(@RequestBody Major major) {
        int result = majorServiceImpl.insert(major);
        if (result > 0) {
            return new Result<>(true, StatusCode.OK, "新增成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "新增失败");
    }

    /**
     * 修改一条数据
     *
     * @param major 实体类
     * @return Result对象
     */
    @PutMapping(value = "/update")
    public Result update(@RequestBody Major major) {
        Major result = majorServiceImpl.update(major);
        if (Objects.nonNull(result)) {
            return new Result<>(true, StatusCode.OK, "修改成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "修改失败");
    }

    /**
     * 删除一条数据
     *
     * @param mId 主键
     * @return Result对象
     */
    @DeleteMapping(value = "/delete/{mId}")
    public Result delete(@PathVariable("mId") Integer mId) {
        int result = majorServiceImpl.deleteById(mId);
        if (result > 0) {
            return new Result<>(true, StatusCode.OK, "删除成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "删除失败");
    }

    /**
     * 查询全部
     *
     * @return Result对象
     */
    @GetMapping(value = "/selectAll")
    public Result<List<Major>> selectAll() {
        List<Major> majors = majorServiceImpl.selectAll();
        if (CollectionUtils.isEmpty(majors)) {
            return new Result<>(true, StatusCode.ERROR, "查询全部数据失败");
        }
        return new Result<>(true, StatusCode.OK, "查询全部数据成功", majors);

    }

    /**
     * 分页查询
     *
     * @param current 当前页  第零页和第一页的数据是一样
     * @param size    每一页的数据条数
     * @return Result对象
     */
    @GetMapping(value = "/selectPage/{current}/{size}")
    public Result selectPage(@PathVariable("current") Integer current, @PathVariable("size") Integer size) {
        PageInfo<Major> page = majorServiceImpl.selectPage(current, size);
        if (Objects.nonNull(page)) {
            return new Result<>(true, StatusCode.OK, "分页条件查询成功", new PageResult<>(page.getTotal(), page.getList()));
        }
        return new Result<>(true, StatusCode.ERROR, "分页查询数据失败");
    }

}