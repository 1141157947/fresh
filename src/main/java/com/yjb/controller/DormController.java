package com.yjb.controller;

import com.github.pagehelper.PageInfo;
import com.yjb.entity.Card;
import com.yjb.entity.Dorm;
import com.yjb.response.PageResult;
import com.yjb.response.Result;
import com.yjb.response.StatusCode;
import com.yjb.service.DormService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * (Dorm)控制层
 *
 * @author protagonist
 * @since 2021-01-18 20:46:10
 */
@RestController
@Slf4j
@RequestMapping("/dorm")
public class DormController {
    /**
     * 服务对象
     */
    @Resource
    private DormService dormServiceImpl;

    /**
     * 通过主键查询单条数据
     *
     * @param dormId 主键
     * @return 单条数据
     */
    @GetMapping(value = "/get/{dormId}")
    public Result selectOne(@PathVariable("dormId") Integer dormId) {
        Dorm result = dormServiceImpl.selectById(dormId);
        if (Objects.nonNull(result)) {
            return new Result<>(true, StatusCode.OK, "查询成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "查询失败");
    }

    /**
     * 新增一条数据
     *
     * @param dorm 实体类
     * @return Result对象
     */
    @PostMapping(value = "/insert")
    public Result insert(@RequestBody Dorm dorm) {
        int result = dormServiceImpl.insert(dorm);
        if (result > 0) {
            return new Result<>(true, StatusCode.OK, "新增成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "新增失败");
    }

    /**
     * 修改一条数据
     *
     * @param dorm 实体类
     * @return Result对象
     */
    @PutMapping(value = "/update")
    public Result update(@RequestBody Dorm dorm) {
//        System.out.println(dorm);
        Dorm result = dormServiceImpl.update(dorm);
        if (Objects.nonNull(result)) {
            return new Result<>(true, StatusCode.OK, "修改成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "修改失败");
    }

    /**
     * 删除一条数据
     *
     * @param dormId 主键
     * @return Result对象
     */
    @DeleteMapping(value = "/delete/{dormId}")
    public Result delete(@PathVariable("dormId") Integer dormId) {
        int result = dormServiceImpl.deleteById(dormId);
        if (result > 0) {
            return new Result<>(true, StatusCode.OK, "删除成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "删除失败");
    }

    /**
     * 查询全部
     *
     * @return Result对象
     */
    @GetMapping(value = "/selectAll")
    public Result<List<Dorm>> selectAll() {
        List<Dorm> dorms = dormServiceImpl.selectAll();
        if (CollectionUtils.isEmpty(dorms)) {
            return new Result<>(true, StatusCode.ERROR, "查询全部数据失败");
        }
        return new Result<>(true, StatusCode.OK, "查询全部数据成功", dorms);

    }

    /**
     * 分页查询
     *
     * @param current 当前页  第零页和第一页的数据是一样
     * @param size    每一页的数据条数
     * @return Result对象
     */
    @GetMapping(value = "/selectPage/{current}/{size}")
    public Result selectPage(@PathVariable("current") Integer current, @PathVariable("size") Integer size) {
        PageInfo<Dorm> page = dormServiceImpl.selectPage(current, size);
        if (Objects.nonNull(page)) {
            return new Result<>(true, StatusCode.OK, "分页条件查询成功", new PageResult<>(page.getTotal(), page.getList()));
        }
        return new Result<>(true, StatusCode.ERROR, "分页查询数据失败");
    }

    /**
     * 按条件查询查询全部，number是小于，不是等于的
     *
     * @return Result对象
     */
    @PostMapping(value = "/selectList")
    public Result<List<Dorm>> selectList(@RequestBody Dorm dorm) {
//        System.out.println(dorm);
        List<Dorm> dorms = dormServiceImpl.selectList(dorm);
//        System.out.println(dorms);
        if (CollectionUtils.isEmpty(dorms)) {
            return new Result<>(true, StatusCode.ERROR, "查询全部数据失败");
        }
        return new Result<>(true, StatusCode.OK, "查询全部数据成功", dorms);

    }

}