package com.yjb.controller;

import com.github.pagehelper.PageInfo;
import com.yjb.entity.Card;
import com.yjb.response.PageResult;
import com.yjb.response.Result;
import com.yjb.response.StatusCode;
import com.yjb.service.CardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * (Card)控制层
 *
 * @author protagonist
 * @since 2021-01-18 20:46:04
 */
@RestController
@Slf4j
@RequestMapping("/card")
public class CardController {
    /**
     * 服务对象
     */
    @Resource
    private CardService cardServiceImpl;

    /**
     * 通过主键查询单条数据
     *
     * @param cId 主键
     * @return 单条数据
     */
    @GetMapping(value = "/get/{cId}")
    public Result selectOne(@PathVariable("cId") Integer cId) {
        Card result = cardServiceImpl.selectById(cId);
        if (Objects.nonNull(result)) {
            return new Result<>(true, StatusCode.OK, "查询成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "查询失败");
    }

    /**
     * 新增一条数据
     *
     * @param card 实体类
     * @return Result对象
     */
    @PostMapping(value = "/insert")
    public Result insert(@RequestBody Card card) {
        int result = cardServiceImpl.insert(card);
        if (result > 0) {
            return new Result<>(true, StatusCode.OK, "新增成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "新增失败");
    }

    /**
     * 修改一条数据
     *
     * @param card 实体类
     * @return Result对象
     */
    @PutMapping(value = "/update")
    public Result update(@RequestBody Card card) {
        Card result = cardServiceImpl.update(card);
        if (Objects.nonNull(result)) {
            return new Result<>(true, StatusCode.OK, "修改成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "修改失败");
    }

    /**
     * 删除一条数据
     *
     * @param cId 主键
     * @return Result对象
     */
    @DeleteMapping(value = "/delete/{cId}")
    public Result delete(@PathVariable("cId") Integer cId) {
        int result = cardServiceImpl.deleteById(cId);
        if (result > 0) {
            return new Result<>(true, StatusCode.OK, "删除成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "删除失败");
    }

    /**
     * 查询全部
     *
     * @return Result对象
     */
    @GetMapping(value = "/selectAll")
    public Result<List<Card>> selectAll() {
        List<Card> cards = cardServiceImpl.selectAll();
        if (CollectionUtils.isEmpty(cards)) {
            return new Result<>(true, StatusCode.ERROR, "查询全部数据失败");
        }
        return new Result<>(true, StatusCode.OK, "查询全部数据成功", cards);

    }

    /**
     * 分页查询
     *
     * @param current 当前页  第零页和第一页的数据是一样
     * @param size    每一页的数据条数
     * @return Result对象
     */
    @GetMapping(value = "/selectPage/{current}/{size}")
    public Result selectPage(@PathVariable("current") Integer current, @PathVariable("size") Integer size) {
        PageInfo<Card> page = cardServiceImpl.selectPage(current, size);
        if (Objects.nonNull(page)) {
            return new Result<>(true, StatusCode.OK, "分页条件查询成功", new PageResult<>(page.getTotal(), page.getList()));
        }
        return new Result<>(true, StatusCode.ERROR, "分页查询数据失败");
    }


    /**
     * 按条件查询查询全部
     *
     * @return Result对象
     */
    @PostMapping(value = "/selectList")
    public Result<List<Card>> selectList(@RequestBody Card card) {
        System.out.println(card);
        List<Card> cards = cardServiceImpl.selectList(card);
        System.out.println(cards);
        if (CollectionUtils.isEmpty(cards)) {
            return new Result<>(true, StatusCode.ERROR, "查询全部数据失败");
        }
        return new Result<>(true, StatusCode.OK, "查询全部数据成功", cards);

    }

}