package com.yjb.controller;

import com.github.pagehelper.PageInfo;
import com.yjb.entity.Guider;
import com.yjb.entity.User;
import com.yjb.entity.UserVO;
import com.yjb.response.PageResult;
import com.yjb.response.Result;
import com.yjb.response.StatusCode;
import com.yjb.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

/**
 * (User)控制层
 *
 * @author protagonist
 * @since 2021-01-18 20:47:04
 */
@RestController
@Slf4j
@RequestMapping("/user")
public class UserController {
    /**
     * 服务对象
     */
    @Resource
    private UserService userServiceImpl;

    /**
     * 通过主键查询单条数据
     *
     * @param uId 主键
     * @return 单条数据
     */
    @GetMapping(value = "/get/{uId}")
    public Result selectOne(@PathVariable("uId") String uId) {
        User result = userServiceImpl.selectById(uId);
        if (Objects.nonNull(result)) {
            return new Result<>(true, StatusCode.OK, "查询成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "查询失败");
    }

    /**
     * 新增一条数据
     *
     * @param user 实体类
     * @return Result对象
     */
    @PostMapping(value = "/insert")
    public Result insert(@RequestBody User user) {
        int result = userServiceImpl.insert(user);
        if (result > 0) {
            return new Result<>(true, StatusCode.OK, "新增成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "新增失败");
    }

    /**
     * 修改一条数据
     *
     * @param user 实体类
     * @return Result对象
     */
    @PutMapping(value = "/update")
    public Result update(@RequestBody User user) {
        User result = userServiceImpl.update(user);
        if (Objects.nonNull(result)) {
            return new Result<>(true, StatusCode.OK, "修改成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "修改失败");
    }

    /**
     * 删除一条数据
     *
     * @param uId 主键
     * @return Result对象
     */
    @DeleteMapping(value = "/delete/{uId}")
    public Result delete(@PathVariable("uId") String uId) {
        int result = userServiceImpl.deleteById(uId);
        if (result > 0) {
            return new Result<>(true, StatusCode.OK, "删除成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "删除失败");
    }

    /**
     * 查询全部
     *
     * @return Result对象
     */
    @GetMapping(value = "/selectAll")
    public Result<List<User>> selectAll() {
        List<User> users = userServiceImpl.selectAll();
        if (CollectionUtils.isEmpty(users)) {
            return new Result<>(true, StatusCode.ERROR, "查询全部数据失败");
        }
        return new Result<>(true, StatusCode.OK, "查询全部数据成功", users);

    }

    /**
     * 分页查询
     *
     * @param current 当前页  第零页和第一页的数据是一样
     * @param size    每一页的数据条数
     * @return Result对象
     */
    @GetMapping(value = "/selectPage/{current}/{size}")
    public Result selectPage(@PathVariable("current") Integer current, @PathVariable("size") Integer size) {
        PageInfo<User> page = userServiceImpl.selectPage(current, size);
        if (Objects.nonNull(page)) {
            return new Result<>(true, StatusCode.OK, "分页条件查询成功", new PageResult<>(page.getTotal(), page.getList()));
        }
        return new Result<>(true, StatusCode.ERROR, "分页查询数据失败");
    }



    /**
     * 通过主键查询单条数据
     *
     * @param uId 主键
     * @return 单条数据
     */
    @GetMapping(value = "/getIncludeAll/{uId}")
    public Result getIncludeAll(@PathVariable("uId") String uId) {
        UserVO result = userServiceImpl.selectIncludeById(uId);
        if (Objects.nonNull(result)) {
            return new Result<>(true, StatusCode.OK, "查询成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "查询失败");
    }


    /**
     * 查询全部
     *
     * @return Result对象
     */
    @GetMapping(value = "/selectIncludeAll")
    public Result<List<UserVO>> selectIncludeAll() {
        List<UserVO> userVOs = userServiceImpl.selectIncludeAll();
        if (CollectionUtils.isEmpty(userVOs)) {
            return new Result<>(true, StatusCode.ERROR, "查询全部数据失败");
        }
        return new Result<>(true, StatusCode.OK, "查询全部数据成功", userVOs);

    }


    //    登录,用Map接收json格式的数据
    @PostMapping("/login")
    public Result<User> login(@RequestBody LinkedHashMap map){
        System.out.println(map);
        String u_id= (String) map.get("u_id");
        String password= (String) map.get("password");

        User user = userServiceImpl.selectById(u_id);

        //统一返回
        if (user==null){
            return new Result<>(true, StatusCode.ERROR, "没有该用户");
        }else if(!user.getUPassword().equals(password)){
            System.out.println(user.getUPassword());
            System.out.println(password);
            return new Result<>(true, StatusCode.ERROR, "密码错误");
        }
        return new Result<>(true, StatusCode.OK, "登录成功", user);
    }



    /**
     * 按条件查询全部
     *
     * @return Result对象
     */
    @PostMapping(value = "/selectList")
    public Result<List<User>> selectList(@RequestBody User user) {
        System.out.println(user);
        List<User> users = userServiceImpl.selectList(user);
        System.out.println(users);
        if (CollectionUtils.isEmpty(users)) {
            return new Result<>(true, StatusCode.ERROR, "查询全部数据失败");
        }
        return new Result<>(true, StatusCode.OK, "查询全部数据成功", users);

    }

}