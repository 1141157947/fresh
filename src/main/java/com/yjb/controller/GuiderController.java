package com.yjb.controller;

import com.github.pagehelper.PageInfo;
import com.yjb.entity.Dorm;
import com.yjb.entity.Guider;
import com.yjb.response.PageResult;
import com.yjb.response.Result;
import com.yjb.response.StatusCode;
import com.yjb.service.GuiderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * (Guider)控制层
 *
 * @author protagonist
 * @since 2021-01-18 20:46:14
 */
@RestController
@Slf4j
@RequestMapping("/guider")
public class GuiderController {
    /**
     * 服务对象
     */
    @Resource
    private GuiderService guiderServiceImpl;

    /**
     * 通过主键查询单条数据
     *
     * @param gId 主键
     * @return 单条数据
     */
    @GetMapping(value = "/get/{gId}")
    public Result selectOne(@PathVariable("gId") String gId) {
        Guider result = guiderServiceImpl.selectById(gId);
        if (Objects.nonNull(result)) {
            return new Result<>(true, StatusCode.OK, "查询成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "查询失败");
    }

    /**
     * 新增一条数据
     *
     * @param guider 实体类
     * @return Result对象
     */
    @PostMapping(value = "/insert")
    public Result insert(@RequestBody Guider guider) {
        int result = guiderServiceImpl.insert(guider);
        if (result > 0) {
            return new Result<>(true, StatusCode.OK, "新增成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "新增失败");
    }

    /**
     * 修改一条数据
     *
     * @param guider 实体类
     * @return Result对象
     */
    @PutMapping(value = "/update")
    public Result update(@RequestBody Guider guider) {
        Guider result = guiderServiceImpl.update(guider);
        if (Objects.nonNull(result)) {
            return new Result<>(true, StatusCode.OK, "修改成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "修改失败");
    }

    /**
     * 删除一条数据
     *
     * @param gId 主键
     * @return Result对象
     */
    @DeleteMapping(value = "/delete/{gId}")
    public Result delete(@PathVariable("gId") String gId) {
        int result = guiderServiceImpl.deleteById(gId);
        if (result > 0) {
            return new Result<>(true, StatusCode.OK, "删除成功", result);
        }
        return new Result<>(true, StatusCode.ERROR, "删除失败");
    }

    /**
     * 查询全部
     *
     * @return Result对象
     */
    @GetMapping(value = "/selectAll")
    public Result<List<Guider>> selectAll() {
        List<Guider> guiders = guiderServiceImpl.selectAll();
        if (CollectionUtils.isEmpty(guiders)) {
            return new Result<>(true, StatusCode.ERROR, "查询全部数据失败");
        }
        return new Result<>(true, StatusCode.OK, "查询全部数据成功", guiders);

    }

    /**
     * 分页查询
     *
     * @param current 当前页  第零页和第一页的数据是一样
     * @param size    每一页的数据条数
     * @return Result对象
     */
    @GetMapping(value = "/selectPage/{current}/{size}")
    public Result selectPage(@PathVariable("current") Integer current, @PathVariable("size") Integer size) {
        PageInfo<Guider> page = guiderServiceImpl.selectPage(current, size);
        if (Objects.nonNull(page)) {
            return new Result<>(true, StatusCode.OK, "分页条件查询成功", new PageResult<>(page.getTotal(), page.getList()));
        }
        return new Result<>(true, StatusCode.ERROR, "分页查询数据失败");
    }

    /**
     * 按条件查询全部
     *
     * @return Result对象
     */
    @PostMapping(value = "/selectList")
    public Result<List<Guider>> selectList(@RequestBody Guider guider) {
        System.out.println(guider);
        List<Guider> guiders = guiderServiceImpl.selectList(guider);
        System.out.println(guiders);
        if (CollectionUtils.isEmpty(guiders)) {
            return new Result<>(true, StatusCode.ERROR, "查询全部数据失败");
        }
        return new Result<>(true, StatusCode.OK, "查询全部数据成功", guiders);

    }

}