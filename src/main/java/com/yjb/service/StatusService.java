package com.yjb.service;

import com.github.pagehelper.PageInfo;
import com.yjb.entity.Status;

import java.util.List;

/**
 * (Status)表服务接口
 *
 * @author protagonist
 * @since 2021-01-18 20:46:23
 */
public interface StatusService {

    /**
     * 通过ID查询单条数据
     *
     * @param uId 主键
     * @return 实例对象
     */
    Status selectById(String uId);

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页数据的条数
     * @return 对象列表
     */
    PageInfo<Status> selectPage(int current, int size);

    /**
     * 查询全部
     *
     * @return 对象列表
     */
    List<Status> selectAll();

    /**
     * 通过实体作为筛选条件查询
     *
     * @param status 实例对象
     * @return 对象列表
     */
    List<Status> selectList(Status status);

    /**
     * 新增数据
     *
     * @param status 实例对象
     * @return 影响行数
     */
    int insert(Status status);

    /**
     * 批量新增
     *
     * @param statuss 实例对象的集合
     * @return 影响行数
     */
    int batchInsert(List<Status> statuss);

    /**
     * 修改数据
     *
     * @param status 实例对象
     * @return 修改
     */
    Status update(Status status);

    /**
     * 通过主键删除数据
     *
     * @param uId 主键
     * @return 影响行数
     */
    int deleteById(String uId);

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    int count();
}