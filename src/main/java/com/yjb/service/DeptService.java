package com.yjb.service;

import com.github.pagehelper.PageInfo;
import com.yjb.entity.Dept;

import java.util.List;

/**
 * (Dept)表服务接口
 *
 * @author protagonist
 * @since 2021-01-18 20:46:08
 */
public interface DeptService {

    /**
     * 通过ID查询单条数据
     *
     * @param dId 主键
     * @return 实例对象
     */
    Dept selectById(Integer dId);

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页数据的条数
     * @return 对象列表
     */
    PageInfo<Dept> selectPage(int current, int size);

    /**
     * 查询全部
     *
     * @return 对象列表
     */
    List<Dept> selectAll();

    /**
     * 通过实体作为筛选条件查询
     *
     * @param dept 实例对象
     * @return 对象列表
     */
    List<Dept> selectList(Dept dept);

    /**
     * 新增数据
     *
     * @param dept 实例对象
     * @return 影响行数
     */
    int insert(Dept dept);

    /**
     * 批量新增
     *
     * @param depts 实例对象的集合
     * @return 影响行数
     */
    int batchInsert(List<Dept> depts);

    /**
     * 修改数据
     *
     * @param dept 实例对象
     * @return 修改
     */
    Dept update(Dept dept);

    /**
     * 通过主键删除数据
     *
     * @param dId 主键
     * @return 影响行数
     */
    int deleteById(Integer dId);

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    int count();
}