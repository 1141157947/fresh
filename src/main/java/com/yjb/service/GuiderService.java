package com.yjb.service;

import com.github.pagehelper.PageInfo;
import com.yjb.entity.Guider;

import java.util.List;

/**
 * (Guider)表服务接口
 *
 * @author protagonist
 * @since 2021-01-18 20:46:13
 */
public interface GuiderService {

    /**
     * 通过ID查询单条数据
     *
     * @param gId 主键
     * @return 实例对象
     */
    Guider selectById(String gId);

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页数据的条数
     * @return 对象列表
     */
    PageInfo<Guider> selectPage(int current, int size);

    /**
     * 查询全部
     *
     * @return 对象列表
     */
    List<Guider> selectAll();

    /**
     * 通过实体作为筛选条件查询
     *
     * @param guider 实例对象
     * @return 对象列表
     */
    List<Guider> selectList(Guider guider);

    /**
     * 新增数据
     *
     * @param guider 实例对象
     * @return 影响行数
     */
    int insert(Guider guider);

    /**
     * 批量新增
     *
     * @param guiders 实例对象的集合
     * @return 影响行数
     */
    int batchInsert(List<Guider> guiders);

    /**
     * 修改数据
     *
     * @param guider 实例对象
     * @return 修改
     */
    Guider update(Guider guider);

    /**
     * 通过主键删除数据
     *
     * @param gId 主键
     * @return 影响行数
     */
    int deleteById(String gId);

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    int count();
}