package com.yjb.service;

import com.github.pagehelper.PageInfo;
import com.yjb.entity.Teacher;

import java.util.List;

/**
 * (Teacher)表服务接口
 *
 * @author protagonist
 * @since 2021-01-18 20:46:25
 */
public interface TeacherService {

    /**
     * 通过ID查询单条数据
     *
     * @param tId 主键
     * @return 实例对象
     */
    Teacher selectById(Integer tId);

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页数据的条数
     * @return 对象列表
     */
    PageInfo<Teacher> selectPage(int current, int size);

    /**
     * 查询全部
     *
     * @return 对象列表
     */
    List<Teacher> selectAll();

    /**
     * 通过实体作为筛选条件查询
     *
     * @param teacher 实例对象
     * @return 对象列表
     */
    List<Teacher> selectList(Teacher teacher);

    /**
     * 新增数据
     *
     * @param teacher 实例对象
     * @return 影响行数
     */
    int insert(Teacher teacher);

    /**
     * 批量新增
     *
     * @param teachers 实例对象的集合
     * @return 影响行数
     */
    int batchInsert(List<Teacher> teachers);

    /**
     * 修改数据
     *
     * @param teacher 实例对象
     * @return 修改
     */
    Teacher update(Teacher teacher);

    /**
     * 通过主键删除数据
     *
     * @param tId 主键
     * @return 影响行数
     */
    int deleteById(Integer tId);

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    int count();
}