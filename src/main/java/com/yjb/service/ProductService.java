package com.yjb.service;

import com.github.pagehelper.PageInfo;
import com.yjb.entity.Product;

import java.util.List;

/**
 * (Product)表服务接口
 *
 * @author protagonist
 * @since 2021-01-18 20:46:17
 */
public interface ProductService {

    /**
     * 通过ID查询单条数据
     *
     * @param pId 主键
     * @return 实例对象
     */
    Product selectById(Integer pId);

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页数据的条数
     * @return 对象列表
     */
    PageInfo<Product> selectPage(int current, int size);

    /**
     * 查询全部
     *
     * @return 对象列表
     */
    List<Product> selectAll();

    /**
     * 通过实体作为筛选条件查询
     *
     * @param product 实例对象
     * @return 对象列表
     */
    List<Product> selectList(Product product);

    /**
     * 新增数据
     *
     * @param product 实例对象
     * @return 影响行数
     */
    int insert(Product product);

    /**
     * 批量新增
     *
     * @param products 实例对象的集合
     * @return 影响行数
     */
    int batchInsert(List<Product> products);

    /**
     * 修改数据
     *
     * @param product 实例对象
     * @return 修改
     */
    Product update(Product product);

    /**
     * 通过主键删除数据
     *
     * @param pId 主键
     * @return 影响行数
     */
    int deleteById(Integer pId);

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    int count();
}