package com.yjb.service;

import com.github.pagehelper.PageInfo;
import com.yjb.entity.Userinfo;

import java.util.List;

/**
 * (Userinfo)表服务接口
 *
 * @author protagonist
 * @since 2021-01-18 20:47:07
 */
public interface UserinfoService {

    /**
     * 通过ID查询单条数据
     *
     * @param uId 主键
     * @return 实例对象
     */
    Userinfo selectById(String uId);

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页数据的条数
     * @return 对象列表
     */
    PageInfo<Userinfo> selectPage(int current, int size);

    /**
     * 查询全部
     *
     * @return 对象列表
     */
    List<Userinfo> selectAll();

    /**
     * 通过实体作为筛选条件查询
     *
     * @param userinfo 实例对象
     * @return 对象列表
     */
    List<Userinfo> selectList(Userinfo userinfo);

    /**
     * 新增数据
     *
     * @param userinfo 实例对象
     * @return 影响行数
     */
    int insert(Userinfo userinfo);

    /**
     * 批量新增
     *
     * @param userinfos 实例对象的集合
     * @return 影响行数
     */
    int batchInsert(List<Userinfo> userinfos);

    /**
     * 修改数据
     *
     * @param userinfo 实例对象
     * @return 修改
     */
    Userinfo update(Userinfo userinfo);

    /**
     * 通过主键删除数据
     *
     * @param uId 主键
     * @return 影响行数
     */
    int deleteById(String uId);

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    int count();
}