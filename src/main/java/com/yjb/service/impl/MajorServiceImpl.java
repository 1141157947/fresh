package com.yjb.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yjb.dao.MajorDao;
import com.yjb.entity.Major;
import com.yjb.service.MajorService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * (Major表)服务实现类
 *
 * @author protagonist
 * @since 2021-01-18 20:46:16
 */
@Service("majorServiceImpl")
public class MajorServiceImpl implements MajorService {
    @Resource
    private MajorDao majorDao;

    /**
     * 通过ID查询单条数据
     *
     * @param mId 主键
     * @return 实例对象
     */
    @Override
    public Major selectById(Integer mId) {
        return this.majorDao.selectById(mId);
    }

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页的条数
     * @return 对象列表
     */
    @Override
    public PageInfo<Major> selectPage(int current, int size) {
        PageHelper.startPage(current, size);
        List<Major> dataList = majorDao.selectAll();
        return new PageInfo<>(dataList);
    }

    /**
     * 查询所有
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Major> selectAll() {
        return this.majorDao.selectAll();
    }

    /**
     * 根据条件查询
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Major> selectList(Major major) {
        return this.majorDao.selectList(major);
    }

    /**
     * 新增数据
     *
     * @param major 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public int insert(Major major) {
        return this.majorDao.insert(major);
    }

    /**
     * 批量新增
     *
     * @param majors 实例对象的集合
     * @return 生效的条数
     */
    @Override
    @Transactional
    public int batchInsert(List<Major> majors) {
        return this.majorDao.batchInsert(majors);
    }

    /**
     * 修改数据
     *
     * @param major 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public Major update(Major major) {
        this.majorDao.update(major);
        return this.selectById(major.getMId());
    }

    /**
     * 通过主键删除数据
     *
     * @param mId 主键
     * @return 是否成功
     */
    @Override
    @Transactional
    public int deleteById(Integer mId) {
        return this.majorDao.deleteById(mId);
    }

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    @Override
    public int count() {
        return this.majorDao.count();
    }
}