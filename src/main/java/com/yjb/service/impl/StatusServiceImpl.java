package com.yjb.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yjb.dao.StatusDao;
import com.yjb.entity.Status;
import com.yjb.service.StatusService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * (Status表)服务实现类
 *
 * @author protagonist
 * @since 2021-01-18 20:46:23
 */
@Service("statusServiceImpl")
public class StatusServiceImpl implements StatusService {
    @Resource
    private StatusDao statusDao;

    /**
     * 通过ID查询单条数据
     *
     * @param uId 主键
     * @return 实例对象
     */
    @Override
    public Status selectById(String uId) {
        return this.statusDao.selectById(uId);
    }

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页的条数
     * @return 对象列表
     */
    @Override
    public PageInfo<Status> selectPage(int current, int size) {
        PageHelper.startPage(current, size);
        List<Status> dataList = statusDao.selectAll();
        return new PageInfo<>(dataList);
    }

    /**
     * 查询所有
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Status> selectAll() {
        return this.statusDao.selectAll();
    }

    /**
     * 根据条件查询
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Status> selectList(Status status) {
        return this.statusDao.selectList(status);
    }

    /**
     * 新增数据
     *
     * @param status 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public int insert(Status status) {
        return this.statusDao.insert(status);
    }

    /**
     * 批量新增
     *
     * @param statuss 实例对象的集合
     * @return 生效的条数
     */
    @Override
    @Transactional
    public int batchInsert(List<Status> statuss) {
        return this.statusDao.batchInsert(statuss);
    }

    /**
     * 修改数据
     *
     * @param status 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public Status update(Status status) {
        this.statusDao.update(status);
        return this.selectById(status.getUId());
    }

    /**
     * 通过主键删除数据
     *
     * @param uId 主键
     * @return 是否成功
     */
    @Override
    @Transactional
    public int deleteById(String uId) {
        return this.statusDao.deleteById(uId);
    }

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    @Override
    public int count() {
        return this.statusDao.count();
    }
}