package com.yjb.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yjb.dao.TrainDao;
import com.yjb.entity.Train;
import com.yjb.service.TrainService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * (Train表)服务实现类
 *
 * @author protagonist
 * @since 2021-01-21 15:32:34
 */
@Service("trainServiceImpl")
public class TrainServiceImpl implements TrainService {
    @Resource
    private TrainDao trainDao;

    /**
     * 通过ID查询单条数据
     *
     * @param uId 主键
     * @return 实例对象
     */
    @Override
    public Train selectById(String uId) {
        return this.trainDao.selectById(uId);
    }

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页的条数
     * @return 对象列表
     */
    @Override
    public PageInfo<Train> selectPage(int current, int size) {
        PageHelper.startPage(current, size);
        List<Train> dataList = trainDao.selectAll();
        return new PageInfo<>(dataList);
    }

    /**
     * 查询所有
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Train> selectAll() {
        return this.trainDao.selectAll();
    }

    /**
     * 根据条件查询
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Train> selectList(Train train) {
        return this.trainDao.selectList(train);
    }

    /**
     * 新增数据
     *
     * @param train 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public int insert(Train train) {
        return this.trainDao.insert(train);
    }

    /**
     * 批量新增
     *
     * @param trains 实例对象的集合
     * @return 生效的条数
     */
    @Override
    @Transactional
    public int batchInsert(List<Train> trains) {
        return this.trainDao.batchInsert(trains);
    }

    /**
     * 修改数据
     *
     * @param train 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public Train update(Train train) {
        this.trainDao.update(train);
        return this.selectById(train.getUId());
    }

    /**
     * 通过主键删除数据
     *
     * @param uId 主键
     * @return 是否成功
     */
    @Override
    @Transactional
    public int deleteById(String uId) {
        return this.trainDao.deleteById(uId);
    }

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    @Override
    public int count() {
        return this.trainDao.count();
    }
}