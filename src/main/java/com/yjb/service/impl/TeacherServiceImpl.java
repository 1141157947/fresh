package com.yjb.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yjb.dao.TeacherDao;
import com.yjb.entity.Teacher;
import com.yjb.service.TeacherService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * (Teacher表)服务实现类
 *
 * @author protagonist
 * @since 2021-01-18 20:46:26
 */
@Service("teacherServiceImpl")
public class TeacherServiceImpl implements TeacherService {
    @Resource
    private TeacherDao teacherDao;

    /**
     * 通过ID查询单条数据
     *
     * @param tId 主键
     * @return 实例对象
     */
    @Override
    public Teacher selectById(Integer tId) {
        return this.teacherDao.selectById(tId);
    }

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页的条数
     * @return 对象列表
     */
    @Override
    public PageInfo<Teacher> selectPage(int current, int size) {
        PageHelper.startPage(current, size);
        List<Teacher> dataList = teacherDao.selectAll();
        return new PageInfo<>(dataList);
    }

    /**
     * 查询所有
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Teacher> selectAll() {
        return this.teacherDao.selectAll();
    }

    /**
     * 根据条件查询
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Teacher> selectList(Teacher teacher) {
        return this.teacherDao.selectList(teacher);
    }

    /**
     * 新增数据
     *
     * @param teacher 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public int insert(Teacher teacher) {
        return this.teacherDao.insert(teacher);
    }

    /**
     * 批量新增
     *
     * @param teachers 实例对象的集合
     * @return 生效的条数
     */
    @Override
    @Transactional
    public int batchInsert(List<Teacher> teachers) {
        return this.teacherDao.batchInsert(teachers);
    }

    /**
     * 修改数据
     *
     * @param teacher 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public Teacher update(Teacher teacher) {
        this.teacherDao.update(teacher);
        return this.selectById(teacher.getTId());
    }

    /**
     * 通过主键删除数据
     *
     * @param tId 主键
     * @return 是否成功
     */
    @Override
    @Transactional
    public int deleteById(Integer tId) {
        return this.teacherDao.deleteById(tId);
    }

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    @Override
    public int count() {
        return this.teacherDao.count();
    }
}