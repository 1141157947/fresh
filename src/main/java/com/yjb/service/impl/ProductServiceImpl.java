package com.yjb.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yjb.dao.ProductDao;
import com.yjb.entity.Product;
import com.yjb.service.ProductService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * (Product表)服务实现类
 *
 * @author protagonist
 * @since 2021-01-18 20:46:18
 */
@Service("productServiceImpl")
public class ProductServiceImpl implements ProductService {
    @Resource
    private ProductDao productDao;

    /**
     * 通过ID查询单条数据
     *
     * @param pId 主键
     * @return 实例对象
     */
    @Override
    public Product selectById(Integer pId) {
        return this.productDao.selectById(pId);
    }

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页的条数
     * @return 对象列表
     */
    @Override
    public PageInfo<Product> selectPage(int current, int size) {
        PageHelper.startPage(current, size);
        List<Product> dataList = productDao.selectAll();
        return new PageInfo<>(dataList);
    }

    /**
     * 查询所有
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Product> selectAll() {
        return this.productDao.selectAll();
    }

    /**
     * 根据条件查询
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Product> selectList(Product product) {
        return this.productDao.selectList(product);
    }

    /**
     * 新增数据
     *
     * @param product 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public int insert(Product product) {
        return this.productDao.insert(product);
    }

    /**
     * 批量新增
     *
     * @param products 实例对象的集合
     * @return 生效的条数
     */
    @Override
    @Transactional
    public int batchInsert(List<Product> products) {
        return this.productDao.batchInsert(products);
    }

    /**
     * 修改数据
     *
     * @param product 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public Product update(Product product) {
        this.productDao.update(product);
        return this.selectById(product.getPId());
    }

    /**
     * 通过主键删除数据
     *
     * @param pId 主键
     * @return 是否成功
     */
    @Override
    @Transactional
    public int deleteById(Integer pId) {
        return this.productDao.deleteById(pId);
    }

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    @Override
    public int count() {
        return this.productDao.count();
    }
}