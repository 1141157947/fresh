package com.yjb.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yjb.dao.GuiderDao;
import com.yjb.entity.Guider;
import com.yjb.service.GuiderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * (Guider表)服务实现类
 *
 * @author protagonist
 * @since 2021-01-18 20:46:13
 */
@Service("guiderServiceImpl")
public class GuiderServiceImpl implements GuiderService {
    @Resource
    private GuiderDao guiderDao;

    /**
     * 通过ID查询单条数据
     *
     * @param gId 主键
     * @return 实例对象
     */
    @Override
    public Guider selectById(String gId) {
        return this.guiderDao.selectById(gId);
    }

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页的条数
     * @return 对象列表
     */
    @Override
    public PageInfo<Guider> selectPage(int current, int size) {
        PageHelper.startPage(current, size);
        List<Guider> dataList = guiderDao.selectAll();
        return new PageInfo<>(dataList);
    }

    /**
     * 查询所有
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Guider> selectAll() {
        return this.guiderDao.selectAll();
    }

    /**
     * 根据条件查询
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Guider> selectList(Guider guider) {
        return this.guiderDao.selectList(guider);
    }

    /**
     * 新增数据
     *
     * @param guider 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public int insert(Guider guider) {
        return this.guiderDao.insert(guider);
    }

    /**
     * 批量新增
     *
     * @param guiders 实例对象的集合
     * @return 生效的条数
     */
    @Override
    @Transactional
    public int batchInsert(List<Guider> guiders) {
        return this.guiderDao.batchInsert(guiders);
    }

    /**
     * 修改数据
     *
     * @param guider 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public Guider update(Guider guider) {
        this.guiderDao.update(guider);
        return this.selectById(guider.getGId());
    }

    /**
     * 通过主键删除数据
     *
     * @param gId 主键
     * @return 是否成功
     */
    @Override
    @Transactional
    public int deleteById(String gId) {
        return this.guiderDao.deleteById(gId);
    }

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    @Override
    public int count() {
        return this.guiderDao.count();
    }
}