package com.yjb.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yjb.dao.ClassesDao;
import com.yjb.entity.Classes;
import com.yjb.service.ClassesService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * (Classes表)服务实现类
 *
 * @author protagonist
 * @since 2021-01-18 20:46:06
 */
@Service("classesServiceImpl")
public class ClassesServiceImpl implements ClassesService {
    @Resource
    private ClassesDao classesDao;

    /**
     * 通过ID查询单条数据
     *
     * @param cId 主键
     * @return 实例对象
     */
    @Override
    public Classes selectById(String cId) {
        return this.classesDao.selectById(cId);
    }

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页的条数
     * @return 对象列表
     */
    @Override
    public PageInfo<Classes> selectPage(int current, int size) {
        PageHelper.startPage(current, size);
        List<Classes> dataList = classesDao.selectAll();
        return new PageInfo<>(dataList);
    }

    /**
     * 查询所有
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Classes> selectAll() {
        return this.classesDao.selectAll();
    }

    /**
     * 根据条件查询
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Classes> selectList(Classes classes) {
        return this.classesDao.selectList(classes);
    }

    /**
     * 新增数据
     *
     * @param classes 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public int insert(Classes classes) {
        return this.classesDao.insert(classes);
    }

    /**
     * 批量新增
     *
     * @param classess 实例对象的集合
     * @return 生效的条数
     */
    @Override
    @Transactional
    public int batchInsert(List<Classes> classess) {
        return this.classesDao.batchInsert(classess);
    }

    /**
     * 修改数据
     *
     * @param classes 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public Classes update(Classes classes) {
        this.classesDao.update(classes);
        return this.selectById(classes.getCId());
    }

    /**
     * 通过主键删除数据
     *
     * @param cId 主键
     * @return 是否成功
     */
    @Override
    @Transactional
    public int deleteById(String cId) {
        return this.classesDao.deleteById(cId);
    }

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    @Override
    public int count() {
        return this.classesDao.count();
    }
}