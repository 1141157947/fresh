package com.yjb.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yjb.dao.DeptDao;
import com.yjb.entity.Dept;
import com.yjb.service.DeptService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * (Dept表)服务实现类
 *
 * @author protagonist
 * @since 2021-01-18 20:46:08
 */
@Service("deptServiceImpl")
public class DeptServiceImpl implements DeptService {
    @Resource
    private DeptDao deptDao;

    /**
     * 通过ID查询单条数据
     *
     * @param dId 主键
     * @return 实例对象
     */
    @Override
    public Dept selectById(Integer dId) {
        return this.deptDao.selectById(dId);
    }

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页的条数
     * @return 对象列表
     */
    @Override
    public PageInfo<Dept> selectPage(int current, int size) {
        PageHelper.startPage(current, size);
        List<Dept> dataList = deptDao.selectAll();
        return new PageInfo<>(dataList);
    }

    /**
     * 查询所有
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Dept> selectAll() {
        return this.deptDao.selectAll();
    }

    /**
     * 根据条件查询
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Dept> selectList(Dept dept) {
        return this.deptDao.selectList(dept);
    }

    /**
     * 新增数据
     *
     * @param dept 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public int insert(Dept dept) {
        return this.deptDao.insert(dept);
    }

    /**
     * 批量新增
     *
     * @param depts 实例对象的集合
     * @return 生效的条数
     */
    @Override
    @Transactional
    public int batchInsert(List<Dept> depts) {
        return this.deptDao.batchInsert(depts);
    }

    /**
     * 修改数据
     *
     * @param dept 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public Dept update(Dept dept) {
        this.deptDao.update(dept);
        return this.selectById(dept.getDId());
    }

    /**
     * 通过主键删除数据
     *
     * @param dId 主键
     * @return 是否成功
     */
    @Override
    @Transactional
    public int deleteById(Integer dId) {
        return this.deptDao.deleteById(dId);
    }

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    @Override
    public int count() {
        return this.deptDao.count();
    }
}