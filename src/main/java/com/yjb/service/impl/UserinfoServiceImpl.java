package com.yjb.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yjb.dao.UserinfoDao;
import com.yjb.entity.Userinfo;
import com.yjb.service.UserinfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * (Userinfo表)服务实现类
 *
 * @author protagonist
 * @since 2021-01-18 20:47:07
 */
@Service("userinfoServiceImpl")
public class UserinfoServiceImpl implements UserinfoService {
    @Resource
    private UserinfoDao userinfoDao;

    /**
     * 通过ID查询单条数据
     *
     * @param uId 主键
     * @return 实例对象
     */
    @Override
    public Userinfo selectById(String uId) {
        return this.userinfoDao.selectById(uId);
    }

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页的条数
     * @return 对象列表
     */
    @Override
    public PageInfo<Userinfo> selectPage(int current, int size) {
        PageHelper.startPage(current, size);
        List<Userinfo> dataList = userinfoDao.selectAll();
        return new PageInfo<>(dataList);
    }

    /**
     * 查询所有
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Userinfo> selectAll() {
        return this.userinfoDao.selectAll();
    }

    /**
     * 根据条件查询
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Userinfo> selectList(Userinfo userinfo) {
        return this.userinfoDao.selectList(userinfo);
    }

    /**
     * 新增数据
     *
     * @param userinfo 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public int insert(Userinfo userinfo) {
        return this.userinfoDao.insert(userinfo);
    }

    /**
     * 批量新增
     *
     * @param userinfos 实例对象的集合
     * @return 生效的条数
     */
    @Override
    @Transactional
    public int batchInsert(List<Userinfo> userinfos) {
        return this.userinfoDao.batchInsert(userinfos);
    }

    /**
     * 修改数据
     *
     * @param userinfo 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public Userinfo update(Userinfo userinfo) {
        this.userinfoDao.update(userinfo);
        return this.selectById(userinfo.getUId());
    }

    /**
     * 通过主键删除数据
     *
     * @param uId 主键
     * @return 是否成功
     */
    @Override
    @Transactional
    public int deleteById(String uId) {
        return this.userinfoDao.deleteById(uId);
    }

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    @Override
    public int count() {
        return this.userinfoDao.count();
    }
}