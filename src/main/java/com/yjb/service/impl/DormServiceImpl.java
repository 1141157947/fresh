package com.yjb.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yjb.dao.DormDao;
import com.yjb.entity.Dorm;
import com.yjb.service.DormService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * (Dorm表)服务实现类
 *
 * @author protagonist
 * @since 2021-01-18 20:46:10
 */
@Service("dormServiceImpl")
public class DormServiceImpl implements DormService {
    @Resource
    private DormDao dormDao;

    /**
     * 通过ID查询单条数据
     *
     * @param dormId 主键
     * @return 实例对象
     */
    @Override
    public Dorm selectById(Integer dormId) {
        return this.dormDao.selectById(dormId);
    }

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页的条数
     * @return 对象列表
     */
    @Override
    public PageInfo<Dorm> selectPage(int current, int size) {
        PageHelper.startPage(current, size);
        List<Dorm> dataList = dormDao.selectAll();
        return new PageInfo<>(dataList);
    }

    /**
     * 查询所有
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Dorm> selectAll() {
        return this.dormDao.selectAll();
    }

    /**
     * 根据条件查询
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Dorm> selectList(Dorm dorm) {
        return this.dormDao.selectList(dorm);
    }

    /**
     * 新增数据
     *
     * @param dorm 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public int insert(Dorm dorm) {
        return this.dormDao.insert(dorm);
    }

    /**
     * 批量新增
     *
     * @param dorms 实例对象的集合
     * @return 生效的条数
     */
    @Override
    @Transactional
    public int batchInsert(List<Dorm> dorms) {
        return this.dormDao.batchInsert(dorms);
    }

    /**
     * 修改数据
     *
     * @param dorm 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public Dorm update(Dorm dorm) {
        this.dormDao.update(dorm);
        return this.selectById(dorm.getDormId());
    }

    /**
     * 通过主键删除数据
     *
     * @param dormId 主键
     * @return 是否成功
     */
    @Override
    @Transactional
    public int deleteById(Integer dormId) {
        return this.dormDao.deleteById(dormId);
    }

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    @Override
    public int count() {
        return this.dormDao.count();
    }
}