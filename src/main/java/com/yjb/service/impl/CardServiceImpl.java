package com.yjb.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yjb.dao.CardDao;
import com.yjb.entity.Card;
import com.yjb.service.CardService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * (Card表)服务实现类
 *
 * @author protagonist
 * @since 2021-01-18 20:46:03
 */
@Service("cardServiceImpl")
public class CardServiceImpl implements CardService {
    @Resource
    private CardDao cardDao;

    /**
     * 通过ID查询单条数据
     *
     * @param cId 主键
     * @return 实例对象
     */
    @Override
    public Card selectById(Integer cId) {
        return this.cardDao.selectById(cId);
    }

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页的条数
     * @return 对象列表
     */
    @Override
    public PageInfo<Card> selectPage(int current, int size) {
        PageHelper.startPage(current, size);
        List<Card> dataList = cardDao.selectAll();
        return new PageInfo<>(dataList);
    }

    /**
     * 查询所有
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Card> selectAll() {
        return this.cardDao.selectAll();
    }

    /**
     * 根据条件查询
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Card> selectList(Card card) {
        return this.cardDao.selectList(card);
    }

    /**
     * 新增数据
     *
     * @param card 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public int insert(Card card) {
        return this.cardDao.insert(card);
    }

    /**
     * 批量新增
     *
     * @param cards 实例对象的集合
     * @return 生效的条数
     */
    @Override
    @Transactional
    public int batchInsert(List<Card> cards) {
        return this.cardDao.batchInsert(cards);
    }

    /**
     * 修改数据
     *
     * @param card 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public Card update(Card card) {
        this.cardDao.update(card);
        return this.selectById(card.getCId());
    }

    /**
     * 通过主键删除数据
     *
     * @param cId 主键
     * @return 是否成功
     */
    @Override
    @Transactional
    public int deleteById(Integer cId) {
        return this.cardDao.deleteById(cId);
    }

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    @Override
    public int count() {
        return this.cardDao.count();
    }
}