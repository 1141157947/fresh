package com.yjb.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yjb.dao.FlowDao;
import com.yjb.entity.Flow;
import com.yjb.service.FlowService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * (Flow表)服务实现类
 *
 * @author protagonist
 * @since 2021-01-18 20:46:11
 */
@Service("flowServiceImpl")
public class FlowServiceImpl implements FlowService {
    @Resource
    private FlowDao flowDao;

    /**
     * 通过ID查询单条数据
     *
     * @param fId 主键
     * @return 实例对象
     */
    @Override
    public Flow selectById(Integer fId) {
        return this.flowDao.selectById(fId);
    }

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页的条数
     * @return 对象列表
     */
    @Override
    public PageInfo<Flow> selectPage(int current, int size) {
        PageHelper.startPage(current, size);
        List<Flow> dataList = flowDao.selectAll();
        return new PageInfo<>(dataList);
    }

    /**
     * 查询所有
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Flow> selectAll() {
        return this.flowDao.selectAll();
    }

    /**
     * 根据条件查询
     *
     * @return 实例对象的集合
     */
    @Override
    public List<Flow> selectList(Flow flow) {
        return this.flowDao.selectList(flow);
    }

    /**
     * 新增数据
     *
     * @param flow 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public int insert(Flow flow) {
        return this.flowDao.insert(flow);
    }

    /**
     * 批量新增
     *
     * @param flows 实例对象的集合
     * @return 生效的条数
     */
    @Override
    @Transactional
    public int batchInsert(List<Flow> flows) {
        return this.flowDao.batchInsert(flows);
    }

    /**
     * 修改数据
     *
     * @param flow 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public Flow update(Flow flow) {
        this.flowDao.update(flow);
        return this.selectById(flow.getFId());
    }

    /**
     * 通过主键删除数据
     *
     * @param fId 主键
     * @return 是否成功
     */
    @Override
    @Transactional
    public int deleteById(Integer fId) {
        return this.flowDao.deleteById(fId);
    }

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    @Override
    public int count() {
        return this.flowDao.count();
    }
}