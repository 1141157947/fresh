package com.yjb.service;

import com.github.pagehelper.PageInfo;
import com.yjb.entity.Dorm;

import java.util.List;

/**
 * (Dorm)表服务接口
 *
 * @author protagonist
 * @since 2021-01-18 20:46:09
 */
public interface DormService {

    /**
     * 通过ID查询单条数据
     *
     * @param dormId 主键
     * @return 实例对象
     */
    Dorm selectById(Integer dormId);

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页数据的条数
     * @return 对象列表
     */
    PageInfo<Dorm> selectPage(int current, int size);

    /**
     * 查询全部
     *
     * @return 对象列表
     */
    List<Dorm> selectAll();

    /**
     * 通过实体作为筛选条件查询
     *
     * @param dorm 实例对象
     * @return 对象列表
     */
    List<Dorm> selectList(Dorm dorm);

    /**
     * 新增数据
     *
     * @param dorm 实例对象
     * @return 影响行数
     */
    int insert(Dorm dorm);

    /**
     * 批量新增
     *
     * @param dorms 实例对象的集合
     * @return 影响行数
     */
    int batchInsert(List<Dorm> dorms);

    /**
     * 修改数据
     *
     * @param dorm 实例对象
     * @return 修改
     */
    Dorm update(Dorm dorm);

    /**
     * 通过主键删除数据
     *
     * @param dormId 主键
     * @return 影响行数
     */
    int deleteById(Integer dormId);

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    int count();
}