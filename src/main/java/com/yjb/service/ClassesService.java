package com.yjb.service;

import com.github.pagehelper.PageInfo;
import com.yjb.entity.Classes;

import java.util.List;

/**
 * (Classes)表服务接口
 *
 * @author protagonist
 * @since 2021-01-18 20:46:06
 */
public interface ClassesService {

    /**
     * 通过ID查询单条数据
     *
     * @param cId 主键
     * @return 实例对象
     */
    Classes selectById(String cId);

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页数据的条数
     * @return 对象列表
     */
    PageInfo<Classes> selectPage(int current, int size);

    /**
     * 查询全部
     *
     * @return 对象列表
     */
    List<Classes> selectAll();

    /**
     * 通过实体作为筛选条件查询
     *
     * @param classes 实例对象
     * @return 对象列表
     */
    List<Classes> selectList(Classes classes);

    /**
     * 新增数据
     *
     * @param classes 实例对象
     * @return 影响行数
     */
    int insert(Classes classes);

    /**
     * 批量新增
     *
     * @param classess 实例对象的集合
     * @return 影响行数
     */
    int batchInsert(List<Classes> classess);

    /**
     * 修改数据
     *
     * @param classes 实例对象
     * @return 修改
     */
    Classes update(Classes classes);

    /**
     * 通过主键删除数据
     *
     * @param cId 主键
     * @return 影响行数
     */
    int deleteById(String cId);

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    int count();
}