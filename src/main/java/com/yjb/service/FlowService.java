package com.yjb.service;

import com.github.pagehelper.PageInfo;
import com.yjb.entity.Flow;

import java.util.List;

/**
 * (Flow)表服务接口
 *
 * @author protagonist
 * @since 2021-01-18 20:46:11
 */
public interface FlowService {

    /**
     * 通过ID查询单条数据
     *
     * @param fId 主键
     * @return 实例对象
     */
    Flow selectById(Integer fId);

    /**
     * 分页查询
     *
     * @param current 当前页
     * @param size    每一页数据的条数
     * @return 对象列表
     */
    PageInfo<Flow> selectPage(int current, int size);

    /**
     * 查询全部
     *
     * @return 对象列表
     */
    List<Flow> selectAll();

    /**
     * 通过实体作为筛选条件查询
     *
     * @param flow 实例对象
     * @return 对象列表
     */
    List<Flow> selectList(Flow flow);

    /**
     * 新增数据
     *
     * @param flow 实例对象
     * @return 影响行数
     */
    int insert(Flow flow);

    /**
     * 批量新增
     *
     * @param flows 实例对象的集合
     * @return 影响行数
     */
    int batchInsert(List<Flow> flows);

    /**
     * 修改数据
     *
     * @param flow 实例对象
     * @return 修改
     */
    Flow update(Flow flow);

    /**
     * 通过主键删除数据
     *
     * @param fId 主键
     * @return 影响行数
     */
    int deleteById(Integer fId);

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    int count();
}