package com.yjb.dao;

import com.yjb.entity.Product;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * (Product)表数据库访问层
 *
 * @author protagonist
 * @since 2021-01-18 20:46:17
 */
@Mapper
public interface ProductDao {

    /**
     * 通过ID查询单条数据
     *
     * @param pId 主键
     * @return 实例对象
     */
    Product selectById(Integer pId);

    /**
     * 查询全部
     *
     * @return 对象列表
     */
    List<Product> selectAll();

    /**
     * 通过实体作为筛选条件查询
     *
     * @param product 实例对象
     * @return 对象列表
     */
    List<Product> selectList(Product product);

    /**
     * 新增数据
     *
     * @param product 实例对象
     * @return 影响行数
     */
    int insert(Product product);

    /**
     * 批量新增
     *
     * @param products 实例对象的集合
     * @return 影响行数
     */
    int batchInsert(List<Product> products);

    /**
     * 修改数据
     *
     * @param product 实例对象
     * @return 影响行数
     */
    int update(Product product);

    /**
     * 通过主键删除数据
     *
     * @param pId 主键
     * @return 影响行数
     */
    int deleteById(Integer pId);

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    int count();
}