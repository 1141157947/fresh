package com.yjb.dao;

import com.yjb.entity.Card;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * (Card)表数据库访问层
 *
 * @author protagonist
 * @since 2021-01-18 20:46:03
 */
@Mapper
public interface CardDao {

    /**
     * 通过ID查询单条数据
     *
     * @param cId 主键
     * @return 实例对象
     */
    Card selectById(Integer cId);

    /**
     * 查询全部
     *
     * @return 对象列表
     */
    List<Card> selectAll();

    /**
     * 通过实体作为筛选条件查询
     *
     * @param card 实例对象
     * @return 对象列表
     */
    List<Card> selectList(Card card);

    /**
     * 新增数据
     *
     * @param card 实例对象
     * @return 影响行数
     */
    int insert(Card card);

    /**
     * 批量新增
     *
     * @param cards 实例对象的集合
     * @return 影响行数
     */
    int batchInsert(List<Card> cards);

    /**
     * 修改数据
     *
     * @param card 实例对象
     * @return 影响行数
     */
    int update(Card card);

    /**
     * 通过主键删除数据
     *
     * @param cId 主键
     * @return 影响行数
     */
    int deleteById(Integer cId);

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    int count();

}