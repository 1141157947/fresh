package com.yjb.dao;

import com.yjb.entity.Major;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * (Major)表数据库访问层
 *
 * @author protagonist
 * @since 2021-01-18 20:46:15
 */
@Mapper
public interface MajorDao {

    /**
     * 通过ID查询单条数据
     *
     * @param mId 主键
     * @return 实例对象
     */
    Major selectById(Integer mId);

    /**
     * 查询全部
     *
     * @return 对象列表
     */
    List<Major> selectAll();

    /**
     * 通过实体作为筛选条件查询
     *
     * @param major 实例对象
     * @return 对象列表
     */
    List<Major> selectList(Major major);

    /**
     * 新增数据
     *
     * @param major 实例对象
     * @return 影响行数
     */
    int insert(Major major);

    /**
     * 批量新增
     *
     * @param majors 实例对象的集合
     * @return 影响行数
     */
    int batchInsert(List<Major> majors);

    /**
     * 修改数据
     *
     * @param major 实例对象
     * @return 影响行数
     */
    int update(Major major);

    /**
     * 通过主键删除数据
     *
     * @param mId 主键
     * @return 影响行数
     */
    int deleteById(Integer mId);

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    int count();
}