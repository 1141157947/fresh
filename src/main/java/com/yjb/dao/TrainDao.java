package com.yjb.dao;

import com.yjb.entity.Train;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * (Train)表数据库访问层
 *
 * @author protagonist
 * @since 2021-01-21 15:32:34
 */
@Mapper
public interface TrainDao {

    /**
     * 通过ID查询单条数据
     *
     * @param uId 主键
     * @return 实例对象
     */
    Train selectById(String uId);

    /**
     * 查询全部
     *
     * @return 对象列表
     */
    List<Train> selectAll();

    /**
     * 通过实体作为筛选条件查询
     *
     * @param train 实例对象
     * @return 对象列表
     */
    List<Train> selectList(Train train);

    /**
     * 新增数据
     *
     * @param train 实例对象
     * @return 影响行数
     */
    int insert(Train train);

    /**
     * 批量新增
     *
     * @param trains 实例对象的集合
     * @return 影响行数
     */
    int batchInsert(List<Train> trains);

    /**
     * 修改数据
     *
     * @param train 实例对象
     * @return 影响行数
     */
    int update(Train train);

    /**
     * 通过主键删除数据
     *
     * @param uId 主键
     * @return 影响行数
     */
    int deleteById(String uId);

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    int count();
}