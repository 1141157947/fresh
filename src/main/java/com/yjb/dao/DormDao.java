package com.yjb.dao;

import com.yjb.entity.Dorm;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * (Dorm)表数据库访问层
 *
 * @author protagonist
 * @since 2021-01-18 20:46:09
 */
@Mapper
public interface DormDao {

    /**
     * 通过ID查询单条数据
     *
     * @param dormId 主键
     * @return 实例对象
     */
    Dorm selectById(Integer dormId);

    /**
     * 查询全部
     *
     * @return 对象列表
     */
    List<Dorm> selectAll();

    /**
     * 通过实体作为筛选条件查询
     *
     * @param dorm 实例对象
     * @return 对象列表
     */
    List<Dorm> selectList(Dorm dorm);

    /**
     * 新增数据
     *
     * @param dorm 实例对象
     * @return 影响行数
     */
    int insert(Dorm dorm);

    /**
     * 批量新增
     *
     * @param dorms 实例对象的集合
     * @return 影响行数
     */
    int batchInsert(List<Dorm> dorms);

    /**
     * 修改数据
     *
     * @param dorm 实例对象
     * @return 影响行数
     */
    int update(Dorm dorm);

    /**
     * 通过主键删除数据
     *
     * @param dormId 主键
     * @return 影响行数
     */
    int deleteById(Integer dormId);

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    int count();
}