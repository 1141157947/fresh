package com.yjb.dao;

import com.yjb.entity.Dept;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * (Dept)表数据库访问层
 *
 * @author protagonist
 * @since 2021-01-18 20:46:08
 */
@Mapper
public interface DeptDao {

    /**
     * 通过ID查询单条数据
     *
     * @param dId 主键
     * @return 实例对象
     */
    Dept selectById(Integer dId);

    /**
     * 查询全部
     *
     * @return 对象列表
     */
    List<Dept> selectAll();

    /**
     * 通过实体作为筛选条件查询
     *
     * @param dept 实例对象
     * @return 对象列表
     */
    List<Dept> selectList(Dept dept);

    /**
     * 新增数据
     *
     * @param dept 实例对象
     * @return 影响行数
     */
    int insert(Dept dept);

    /**
     * 批量新增
     *
     * @param depts 实例对象的集合
     * @return 影响行数
     */
    int batchInsert(List<Dept> depts);

    /**
     * 修改数据
     *
     * @param dept 实例对象
     * @return 影响行数
     */
    int update(Dept dept);

    /**
     * 通过主键删除数据
     *
     * @param dId 主键
     * @return 影响行数
     */
    int deleteById(Integer dId);

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    int count();
}