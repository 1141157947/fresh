package com.yjb.dao;

import com.yjb.entity.Status;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * (Status)表数据库访问层
 *
 * @author protagonist
 * @since 2021-01-18 20:46:22
 */
@Mapper
public interface StatusDao {

    /**
     * 通过ID查询单条数据
     *
     * @param uId 主键
     * @return 实例对象
     */
    Status selectById(String uId);

    /**
     * 查询全部
     *
     * @return 对象列表
     */
    List<Status> selectAll();

    /**
     * 通过实体作为筛选条件查询
     *
     * @param status 实例对象
     * @return 对象列表
     */
    List<Status> selectList(Status status);

    /**
     * 新增数据
     *
     * @param status 实例对象
     * @return 影响行数
     */
    int insert(Status status);

    /**
     * 批量新增
     *
     * @param statuss 实例对象的集合
     * @return 影响行数
     */
    int batchInsert(List<Status> statuss);

    /**
     * 修改数据
     *
     * @param status 实例对象
     * @return 影响行数
     */
    int update(Status status);

    /**
     * 通过主键删除数据
     *
     * @param uId 主键
     * @return 影响行数
     */
    int deleteById(String uId);

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    int count();
}