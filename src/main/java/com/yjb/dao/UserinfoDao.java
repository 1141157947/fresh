package com.yjb.dao;

import com.yjb.entity.Userinfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * (Userinfo)表数据库访问层
 *
 * @author protagonist
 * @since 2021-01-18 20:47:07
 */
@Mapper
public interface UserinfoDao {

    /**
     * 通过ID查询单条数据
     *
     * @param uId 主键
     * @return 实例对象
     */
    Userinfo selectById(String uId);

    /**
     * 查询全部
     *
     * @return 对象列表
     */
    List<Userinfo> selectAll();

    /**
     * 通过实体作为筛选条件查询
     *
     * @param userinfo 实例对象
     * @return 对象列表
     */
    List<Userinfo> selectList(Userinfo userinfo);

    /**
     * 新增数据
     *
     * @param userinfo 实例对象
     * @return 影响行数
     */
    int insert(Userinfo userinfo);

    /**
     * 批量新增
     *
     * @param userinfos 实例对象的集合
     * @return 影响行数
     */
    int batchInsert(List<Userinfo> userinfos);

    /**
     * 修改数据
     *
     * @param userinfo 实例对象
     * @return 影响行数
     */
    int update(Userinfo userinfo);

    /**
     * 通过主键删除数据
     *
     * @param uId 主键
     * @return 影响行数
     */
    int deleteById(String uId);

    /**
     * 查询总数据数
     *
     * @return 数据总数
     */
    int count();
}