package com.yjb.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * (Flow)实体类
 *
 * @author
 * @since 2021-01-20 10:20:38
 */
@Data
public class Flow implements Serializable {
    private static final long serialVersionUID = -88363254992200067L;

    @JsonProperty("fId")
    private Integer fId;

    @JsonProperty("fDate")
    private Object fDate;

    @JsonProperty("fTime")
    private String fTime;

    @JsonProperty("fPlace")
    private String fPlace;

    @JsonProperty("fDesc")
    private String fDesc;
}