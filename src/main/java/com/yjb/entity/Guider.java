package com.yjb.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * (Guider)实体类
 *
 * @author
 * @since 2021-01-20 10:20:38
 */
@Data
public class Guider implements Serializable {
    private static final long serialVersionUID = 241898498230515021L;

    @JsonProperty("gId")
    private String gId;

    @JsonProperty("gClass")
    private Integer gClass;
}