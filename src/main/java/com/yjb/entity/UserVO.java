package com.yjb.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * (User)实体类
 *
 * @author
 * @since 2021-01-18 20:47:03
 */
@Data
public class UserVO implements Serializable {

    private static final long serialVersionUID = -67911861618094492L;

    @JsonProperty("uId")
    private String uId;

    @JsonProperty("uPassword")
    private String uPassword;

    @JsonProperty("uName")
    private String uName;

    @JsonProperty("uGender")
    private String uGender;

    @JsonProperty("cId")
    private Integer cId;

    @JsonProperty("dId")
    private Integer dId;

    @JsonProperty("mId")
    private Integer mId;

    @JsonProperty("regYear")
    private Object regYear;

    @JsonProperty("checkState")
    private Integer checkState;

    @JsonProperty("tId")
    private Integer tId;

    @JsonProperty("offer")
    private String offer;

    @JsonProperty("dormName")
    private String dormName;

    @JsonProperty("acl")
    private String acl;

    Classes classes;

    Dept dept;

    Teacher teacher;

    Major major;

    Userinfo userinfo;
}
