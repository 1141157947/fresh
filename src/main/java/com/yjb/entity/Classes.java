package com.yjb.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * (Classes)实体类
 *
 * @author
 * @since 2021-01-20 10:57:02
 */
@Data
public class Classes implements Serializable {
    private static final long serialVersionUID = 356740753319680191L;

    @JsonProperty("cId")
    private String cId;

    @JsonProperty("cName")
    private String cName;

    @JsonProperty("dId")
    private Integer dId;

    @JsonProperty("mId")
    private Integer mId;
}