package com.yjb.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * (Product)实体类
 *
 * @author
 * @since 2021-01-20 10:20:39
 */
@Data
public class Product implements Serializable {
    private static final long serialVersionUID = 484275222713351211L;

    @JsonProperty("pId")
    private Integer pId;

    @JsonProperty("pName")
    private String pName;

    @JsonProperty("pPrice")
    private Double pPrice;

    @JsonProperty("description")
    private String description;

    @JsonProperty("picture")
    private String picture;
}