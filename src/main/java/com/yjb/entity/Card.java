package com.yjb.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * (Card)实体类
 *
 * @author
 * @since 2021-01-20 10:20:37
 */
@Data
public class Card implements Serializable {
    private static final long serialVersionUID = -60796027377993877L;

    @JsonProperty("cId")
    private Integer cId;

    @JsonProperty("uId")
    private String uId;
}