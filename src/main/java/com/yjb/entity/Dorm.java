package com.yjb.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * (Dorm)实体类
 *
 * @author
 * @since 2021-01-20 10:20:38
 */
@Data
public class Dorm implements Serializable {
    private static final long serialVersionUID = -42056323682568759L;

    @JsonProperty("dormId")
    private Integer dormId;

    @JsonProperty("dormName")
    private String dormName;

    @JsonProperty("capacity")
    private Integer capacity;

    @JsonProperty("number")
    private Integer number;

    @JsonProperty("cId")
    private Integer cId;

    @JsonProperty("gender")
    private String gender;
}