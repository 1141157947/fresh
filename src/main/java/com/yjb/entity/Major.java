package com.yjb.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * (Major)实体类
 *
 * @author
 * @since 2021-01-20 10:20:38
 */
@Data
public class Major implements Serializable {
    private static final long serialVersionUID = -47410763617897609L;

    @JsonProperty("mId")
    private Integer mId;

    @JsonProperty("mName")
    private String mName;

    @JsonProperty("dId")
    private Integer dId;
}