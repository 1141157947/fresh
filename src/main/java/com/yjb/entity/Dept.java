package com.yjb.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * (Dept)实体类
 *
 * @author
 * @since 2021-01-20 10:20:38
 */
@Data
public class Dept implements Serializable {
    private static final long serialVersionUID = -62288355941003224L;

    @JsonProperty("dId")
    private Integer dId;

    @JsonProperty("dName")
    private String dName;
}