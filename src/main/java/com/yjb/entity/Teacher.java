package com.yjb.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * (Teacher)实体类
 *
 * @author
 * @since 2021-01-20 10:20:39
 */
@Data
public class Teacher implements Serializable {
    private static final long serialVersionUID = -77087652009222364L;

    @JsonProperty("tId")
    private Integer tId;

    @JsonProperty("tName")
    private String tName;

    @JsonProperty("cId")
    private Integer cId;

    @JsonProperty("tPhone")
    private String tPhone;
}