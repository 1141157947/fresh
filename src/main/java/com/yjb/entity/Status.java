package com.yjb.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * (Status)实体类
 *
 * @author
 * @since 2021-01-20 10:20:39
 */
@Data
public class Status implements Serializable {
    private static final long serialVersionUID = -37716476196819689L;

    @JsonProperty("uId")
    private String uId;

    @JsonProperty("userinfoupload")
    private String userinfoupload;

    @JsonProperty("cardapply")
    private String cardapply;

    @JsonProperty("dormapply")
    private String dormapply;

    @JsonProperty("traininfoupload")
    private String traininfoupload;

    @JsonProperty("guiderinfoconfirm")
    private String guiderinfoconfirm;
}