package com.yjb.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * (Userinfo)实体类
 *
 * @author
 * @since 2021-01-20 10:20:39
 */
@Data
public class Userinfo implements Serializable {
    private static final long serialVersionUID = 598815307776232068L;

    @JsonProperty("uId")
    private String uId;

    @JsonProperty("idcard")
    private String idcard;

    @JsonProperty("tel")
    private String tel;

    @JsonProperty("bank")
    private String bank;
}