package com.yjb.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * (User)实体类
 *
 * @author
 * @since 2021-01-20 10:20:39
 */
@Data
public class User implements Serializable {
    private static final long serialVersionUID = 112630776161777622L;

    @JsonProperty("uId")
    private String uId;

    @JsonProperty("uPassword")
    private String uPassword;

    @JsonProperty("uName")
    private String uName;

    @JsonProperty("uGender")
    private String uGender;

    @JsonProperty("cId")
    private Integer cId;

    @JsonProperty("dId")
    private Integer dId;

    @JsonProperty("mId")
    private Integer mId;

    @JsonProperty("regYear")
    private Object regYear;

    @JsonProperty("checkState")
    private Integer checkState;

    @JsonProperty("tId")
    private Integer tId;

    @JsonProperty("offer")
    private String offer;

    @JsonProperty("dormName")
    private String dormName;

    @JsonProperty("acl")
    private String acl;
}