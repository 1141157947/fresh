package com.yjb.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * (Train)实体类
 *
 * @author
 * @since 2021-01-21 15:32:34
 */
@Data
public class Train implements Serializable {
    private static final long serialVersionUID = -89672025665611870L;

    @JsonProperty("uId")
    private String uId;

    @JsonProperty("clothSize")
    private String clothSize;

    @JsonProperty("trousersSize")
    private String trousersSize;

    @JsonProperty("shoesSize")
    private String shoesSize;

    @JsonProperty("insurance")
    private String insurance;
}