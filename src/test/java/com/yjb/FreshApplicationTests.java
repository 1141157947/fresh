package com.yjb;

import org.junit.jupiter.api.Test;

import org.mockito.internal.util.StringUtil;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

@SpringBootTest
class FreshApplicationTests {

    @Test
    void contextLoads() {
        String s=getId("信息工程四班(信息工程三班导生)");
        System.out.println(s);
    }
    private  String getId(String string){

        String desc=string.substring(string.indexOf("(")+1,string.indexOf(")"));
        String class_name=desc.substring(0,desc.length() - 2);
        return class_name;
    }

}
